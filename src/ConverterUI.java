/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Reveal the converted value in GUI.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.09
 */
public class ConverterUI extends JFrame {

	private UnitConverter unitconverter;
	private JTextField inputField; 
	private JComboBox<Unit> comboBox1;
	private JTextField outputField;
	private JComboBox<Unit> comboBox2;
	private JButton convertButton;
	private JButton clearButton;
	private static boolean isLeft = false;
	private static double input1 = 0;
	private static double input2 = 0;
	/**
	 * Constructor which initialize UnitConverter, setting JFrame properties,
	 * and invoking initComponents.
	 * @param uc is an UnitConverter
	 */
	public ConverterUI( UnitConverter uc) {
		this.unitconverter = uc;
		this.setTitle("Simple Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
		this.pack();	
	}

	/**
	 * Creating the GUI components.
	 */
	public void initComponents() {
		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		inputField = new JTextField(10);
		contents.add( inputField );
		ActionListener enter = new ConvertButtonListener();
		inputField.addActionListener(enter);
		comboBox1 = new JComboBox<Unit>(Volume.values());
		contents.add( comboBox1 );
		JLabel label = new JLabel( " = " );
		contents.add( label );
		outputField = new JTextField(10);
		outputField.addActionListener(enter);
		contents.add( outputField );
		comboBox2 = new JComboBox<Unit>(Volume.values());
		contents.add( comboBox2 );

		convertButton = new JButton("Convert");
		contents.add( convertButton );
		ActionListener convertListener = new ConvertButtonListener( );
		convertButton.addActionListener( convertListener );

		clearButton = new JButton("Clear");
		contents.add( clearButton );
		ActionListener clearListener = new ClearButtonListener( );
		clearButton.addActionListener( clearListener );

		JMenuBar menuBar = new JMenuBar( );
		JMenu menu = new JMenu("Menu Type");
		JMenuItem itemLength = new JMenuItem("Length");
		itemLength.addActionListener(new LengthConvertListener());
		JMenuItem itemArea = new JMenuItem("Area");
		itemArea.addActionListener(new AreaConvertListener());
		JMenuItem itemWeight = new JMenuItem("Weight");
		itemWeight.addActionListener(new WeightConvertListener());
		JMenuItem itemVolume = new JMenuItem("Volume");
		itemVolume.addActionListener(new VolumeConvertListener());
		menu.add(itemLength);
		menu.add(itemArea);
		menu.add(itemWeight);
		menu.add(itemVolume);
		menu.add(new ExitAction());
		menuBar.add(menu);

		this.setJMenuBar(menuBar);
		this.setVisible(true);
	}

	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed. */
		public void actionPerformed( ActionEvent evt ) {
			inputField.setForeground(Color.BLACK);
			double s1;
			if(inputField.getText().equals(""))
				s1 = 0;
			else
				s1 = Double.parseDouble(inputField.getText());
			double s2;
			if(outputField.getText().equals(""))
				s2 = 0;
			else 
				s2  = Double.parseDouble(outputField.getText());
			double s = 0;
			System.out.println("S1: " + s1);
			System.out.println("S2: " + s2);
			System.out.println("Input1: " + input1);
			System.out.println("Input2: " + input2);

			try {
				if(s1 != input1) {
					isLeft = true;
					s = s1;
					double value = s;
					System.out.println("Value " + value);
					if(value<0)
						throw new NumberFormatException();
					input1 = Double.parseDouble(inputField.getText());
					Unit unit1 = (Unit)comboBox1.getSelectedItem();
					Unit unit2 = (Unit)comboBox2.getSelectedItem();
					double result = unitconverter.convert( value, unit1, unit2);
					outputField.setText(String.format("%.5g", result));
					input2 = result;
					
				}

				else if(s2 != input2) {
					isLeft = false;
					s = s2;
					double value = s;
					System.out.println("Value: " + value);
					if(value<0)
						throw new NumberFormatException();
					input2 = Double.parseDouble(outputField.getText());
					Unit unit1 = (Unit)comboBox1.getSelectedItem();
					Unit unit2 = (Unit)comboBox2.getSelectedItem();
					double result = unitconverter.convert( value, unit2, unit1);
					inputField.setText(String.format("%.5g", result));
					input1 = result;
				}
				System.out.println("Input1: " + input1);
				System.out.println("Input2: " + input2);
				System.out.println("--------");
			} catch (NumberFormatException e) {
				if(isLeft) {
					inputField.setForeground(Color.RED);
				}
				else {
					outputField.setForeground(Color.RED);
				}
				JOptionPane.showMessageDialog(null, "Please input valid number", null, JOptionPane.ERROR_MESSAGE);
			}
		}
	}  // end of the inner class

	class ClearButtonListener implements ActionListener {

		public void actionPerformed( ActionEvent evt ) {
			inputField.setText("");
			outputField.setText("");
		}
	}

	class LengthConvertListener implements ActionListener {

		public void actionPerformed( ActionEvent evt ) {
			comboBox1.removeAllItems();
			comboBox2.removeAllItems();
			for(int i=0; i<Length.values().length; i++){
				comboBox1.addItem(Length.values()[i]);
				comboBox2.addItem(Length.values()[i]);
			}
		}	
	}

	class AreaConvertListener implements ActionListener {

		public void actionPerformed( ActionEvent evt ) {
			comboBox1.removeAllItems();
			comboBox2.removeAllItems();
			for(Area v: Area.values()){
				comboBox1.addItem(v);
				comboBox2.addItem(v);
			}
		}	
	}

	class WeightConvertListener implements ActionListener {

		public void actionPerformed( ActionEvent evt ) {
			comboBox1.removeAllItems();
			comboBox2.removeAllItems();
			for(Weight v: Weight.values()){
				comboBox1.addItem(v);
				comboBox2.addItem(v);
			}
		}	
	}

	class VolumeConvertListener implements ActionListener {

		public void actionPerformed( ActionEvent evt ) {
			comboBox1.removeAllItems();
			comboBox2.removeAllItems();
			for(Volume v: Volume.values()) {
				comboBox1.addItem(v);
				comboBox2.addItem(v);
			}
		}	
	}

	class ExitAction extends AbstractAction {
		public ExitAction() {
			super("Exit");
		}
		public void actionPerformed( ActionEvent evt ) {
			System.exit(0);
		}
	}

}
