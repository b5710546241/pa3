/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * Convert a value from one length unit to another.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.08
 */
public enum Length implements Unit {

	METER("Meters", 1.0),
	KILOMETER("Kilometers", 1000.0),
	CENTIMETER("Centimeters", 0.01),
	MILE("Miles", 1609.344),
	FOOT("Feet", 0.30480),
	WA("Wa", 2.0);
	
	private final double value;
	private final String name;
	
	private Length(String name, double value) {
		this.value = value;
		this.name = name;
	}
	
	/**
	 * @return name of Length unit
	 */
	public String toString() {
		return name;
	}

	/**
	 * @param unit we would like to convert
	 * @param amount as value
	 * @return converted value
	 */
	public double convertTo(Unit unit, double amount) {
		return amount*value/unit.getValue();
	}

	/**
	 * @return value per base unit
	 */
	public double getValue() {
		return this.value;
	}
	
}
