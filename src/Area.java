/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */
/**
 * Convert a value from one area unit to another.
 * @author  Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.08
 */
public enum Area implements Unit{

	SQUARE_METER("Square m", 1.0),
	SQUARE_KILOMETER("Square km", 1.0e-6),
	SQUARE_CENTIMETER("Square cm", 10000.0),
	SQUARE_MILE("Square mile", 2.59e+6),
	SQUARE_FOOT("Square foot", 9.2903e-6),
	SQUARE_WA("Square wa", 4.0);
	
	private final double value;
	private final String name;
	
	private Area(String name, double value) {
		this.value = value;
		this.name = name;
	}

	/**
	 * @return name of Length unit
	 */
	public String toString() {
		return name;
	}

	/**
	 * @param unit we would like to convert
	 * @param amount as value
	 * @return converted value
	 */
	public double convertTo(Unit unit, double amount) {
		return amount*value/unit.getValue();
	}

	/**
	 * @return value per base unit
	 */
	public double getValue() {
		return this.value;
	}
}
