/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */
/**
 * Convert a value from one unit to another.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.09
 */
public interface Unit {

	/**
	 * Converting an amount from one unit to another unit. 
	 * @param unit is an unit of the amount
	 * @param amount is an input from UI
	 * @return double ( converted amount )
	 */
	public double convertTo(Unit unit, double amount);
	
	/**
	 * Getting an unit value.
	 * @return double value of unit
	 */
	public double getValue();
	
	/**
	 * Getting an unit name.
	 * @return String of unit name
	 */
	public String toString();
}
