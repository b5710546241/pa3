/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */
/**
 * Convert a value from one weight unit to another.
 * @author  Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.08
 */
public enum Weight implements Unit{

	KILOGRAM("Kilogram", 1.0),
	GRAM("Gram", 0.001),
	MILLIGRAM("Milligram", 1e-6),
	POUND("Pound", 0.453592),
	OUNCE("Ounce", 0.0283495),
	KEED("Keed", 0.1);
	
	private final double value;
	private final String name;
	
	private Weight(String name, double value) {
		this.value = value;
		this.name = name;
	}
	
	/**
	 * @return name of Length unit
	 */
	public String toString() {
		return name;
	}

	/**
	 * @param unit we would like to convert
	 * @param amount as value
	 * @return converted value
	 */
	public double convertTo(Unit unit, double amount) {
		return amount*value/unit.getValue();
	}

	/**
	 * @return value per base unit
	 */
	public double getValue() {
		return this.value;
	}
}
