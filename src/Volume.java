/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */
/**
 * Convert a value from one volume unit to another.
 * @author  Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.08
 */
public enum Volume implements Unit{

	CUBIC_METER("Cubic meter", 1.0),
	CUBIC_CENTIMETER("Cubic centimeter", 1e-6),
	CUBIC_DECIMETER("Cubic decimeter", 1e-3),
	CUBIC_FOOT("Cubic foot", 0.0283168),
	CUBIC_INCH("Cubic inch", 1.6387e-5),
	THUNG("Thung", 0.02);
	
	private final double value;
	private final String name;
	
	private Volume(String name, double value) {
		this.value = value;
		this.name = name;
	}
	
	/**
	 * @return name of Length unit
	 */
	public String toString() {
		return name;
	}

	/**
	 * @param unit we would like to convert
	 * @param amount as value
	 * @return converted value
	 */
	public double convertTo(Unit unit, double amount) {
		return amount*value/unit.getValue();
	}

	/**
	 * @return value per base unit
	 */
	public double getValue() {
		return this.value;
	}
	
}
