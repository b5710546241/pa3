/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */
/**
 * UnitConverter gets a request from UI and converts unit.
 * @author  Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.08
 */
public class UnitConverter {

	/**
	 * Converting from one unit to another unit.
	 * @param amount is an input from UI
	 * @param fromUnit is an unit from comboBox1
	 * @param toUnit is an unit from comboBox2
	 * @return double ( converted amount )
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit){
		return fromUnit.convertTo(toUnit, amount);
	}

	/**
	 * Getting the values of units.
	 * @return array of units in enum
	 */
	public Unit[] getUnits(){
		return null;
	}
}
